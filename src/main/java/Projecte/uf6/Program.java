package Projecte.uf6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//comprarMaquina("C4","NUEVIKA","i90",true);
		//canviaMaquina("NUEVIKA","1.6");
		switchAC("1.6");
		switchAC("C4");
	}



	public static void comprarMaquina(String nomAula, String nomMaquina, String processador, boolean grafica) {
		try {
			JSONParser parser = new JSONParser();
			// obtenemos objeto raiz
			Object obj = parser.parse(new FileReader("aules.json"));

			// el objeto raiz es un array asi que casteamos array
			JSONArray array = (JSONArray) obj;
			// recorremos el array como si fuera una arraylist
			for (Object o : array) {
				// lo que hay dentro del array son jsonobjects asi que casteamos
				JSONObject aula = (JSONObject) o;
				if (aula.get("nom").equals(nomAula)) {//AQUI VEMOS SI MI AULA(CASTEADA) EL VALOR ES EL NOMBRE
					// buscamos el campo maquinas. Es un JSONArray
					JSONArray maquines = (JSONArray) aula.get("maquines");//COJEMOS EL ARRAY DE AULES
					JSONObject comprada= new JSONObject();//CREAMOS LA MAQUINA NUEVA
					
					comprada.put("nom", nomMaquina);//LE METEMOS LOS ATRIBUTOS
					comprada.put("processador", processador);
					comprada.put("grafica", grafica);
					
					maquines.add(comprada);
				}
			}
			 // escribir el objeto RAIZ.
		      FileWriter file = new FileWriter("aules.json");
			  file.write(array.toJSONString());
			  file.flush();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void canviaMaquina(String nomMaquina, String nomAula) {
		try {
			JSONParser parser = new JSONParser();
			// obtenemos objeto raiz
			Object obj = parser.parse(new FileReader("aules.json"));

			// el objeto raiz es un array asi que casteamos array
			JSONArray array = (JSONArray) obj;
			// recorremos el array como si fuera una arraylist
			JSONObject perCanviar= null;
			boolean trobat=false;
			for (Object o : array) {
				// lo que hay dentro del array son jsonobjects asi que casteamos
				JSONObject aula = (JSONObject) o;
					JSONArray maquines = (JSONArray) aula.get("maquines");//COJEMOS EL ARRAY DE AULES
					for(int i=0;i<maquines.size();i++) {
						//el contenido del jsonarray son jsonobjects maquinas
						JSONObject maquina = (JSONObject) maquines.get(i);
						//buscas si es la maquina que buscamos
						//System.out.println(maquina.get("nom"));
						//ME DABA EL ERROR DE QUE AÑADO UN NULL A DONDE LAS MAQUINAS
						if(maquina.get("nom").equals(nomMaquina)) {
							perCanviar= (JSONObject) maquines.get(i);
							maquines.remove(i);//la elimino de aqui la maquina seleccionada
							trobat=true;
							
						}
					}
			
			}
			if(trobat=true) {
				for (Object o : array) {
					JSONObject aula = (JSONObject) o;
					JSONArray maquines = (JSONArray) aula.get("maquines");//COJEMOS EL ARRAY DE AULES
					maquines.add(perCanviar);
				}	
			}
			 // escribir el objeto RAIZ.
		      FileWriter file = new FileWriter("aules.json");
			  file.write(array.toJSONString());
			  file.flush();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//canviar aire acondicionado:
	private static void switchAC(String nomAula) {
		try {
			JSONParser parser = new JSONParser();
			// obtenemos objeto raiz
			Object obj = parser.parse(new FileReader("aules.json"));

			// el objeto raiz es un array asi que casteamos array
			JSONArray array = (JSONArray) obj;
			// recorremos el array como si fuera una arraylist
			for (Object o : array) {
				// lo que hay dentro del array son jsonobjects asi que casteamos
				JSONObject aula = (JSONObject) o;
				if(aula.get("nom").equals(nomAula)){
					boolean a;
					a = (Boolean) aula.get("aireacondicionat");
					if(a==true) {
						aula.put("aireacondicionat", false);
					}else {
						aula.put("aireacondicionat", true);
					}
						
				
				}
			}
			 // escribir el objeto RAIZ.
		      FileWriter file = new FileWriter("aules.json");
			  file.write(array.toJSONString());
			  file.flush();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// el objeto raiz es un array asi que casteamos array
	
		// recorremos el array como si fuera una arraylist
		
	}
}
